import react, { useState, useEffect } from "react";
import SdkConfig from "../../SdkConfig";
import { MatrixClientPeg } from "../../MatrixClientPeg";
import ApplicationsFlowBuilder from "./ApplicationsFlowBuilder";
import AccessibleButton from "../views/elements/AccessibleButton";

export default function FlowBuilder(props) {
    const [flows, setFlows] = useState([]);
    const [profileInfo, setProfileInfo] = useState(null);
    const _getProfileInfo = async () => {
        const cli = MatrixClientPeg.get();
        const userId = cli.getUserId();
        const profileInfo = await cli.getProfileInfo(userId);

        return {
            userId,
            name: profileInfo.displayname,
        };
    };

    useEffect(() => {
        // Create an scoped async function in the hook
        async function fetchConnectedUser() {
            try {
                const profileInfo = await _getProfileInfo();
                setProfileInfo(profileInfo);
                const flowApiSettings = SdkConfig.get()["flow_api_settings"];
                console.log("big fetch");
                if (
                    flowApiSettings &&
                    flowApiSettings.customer_id &&
                    flowApiSettings.api_route_endpoint
                ) {
                    const response = await fetch(
                        `${flowApiSettings.api_route_endpoint}/userflows?user=${profileInfo.userId}&customer=${flowApiSettings.customer_id}`
                    );
                    const data = await response.json();
                    setFlows(data);
                } else if (flowApiSettings.api_route_endpoint) {
                    // console.log("small fetch");

                    const response = await fetch(
                        `${flowApiSettings.api_route_endpoint}/api/v1/userservice/${profileInfo.userId}`
                    );
                    const data = await response.json();
                    if (data && Array.isArray(data) && data.length > 0) {
                        setFlows(data);
                    }
                }
            } catch (ex) {
                console.log("could not fetch profile");
                console.error(ex);
            }
        }
        fetchConnectedUser();
    }, []);
    const handleFlowClick = (flow) => {
        window.open(flow.public_url, "_blank");
    };
    const onExploreCommunityTemplate = async () => {
        const profileInfo = await _getProfileInfo();
        console.log(profileInfo);
        if (profileInfo && profileInfo.userId) {
            window.open(
                "https://start.tekos.co?workspace=flow&userid=" +
                    profileInfo.userId
            );
        }
    };
    const _getButtonCreate = () => {
        return (
            <div>
                <AccessibleButton
                    className="mx_MemberList_invite mx_MemberList_explore_community"
                    onClick={(e) => onExploreCommunityTemplate()}
                    style={{
                        margin: "0",
                        marginTop: "25px",
                        maxWidth: "300px",
                    }}
                >
                    <span>Create a flow instance</span>
                </AccessibleButton>
            </div>
        );
    };
    const _getDetails = () => {
        let details;
        details = (
            <div className="mx_FlowBuilder_cards">
                {flows.map((flow, index) => {
                    return (
                        <div key={index} className="css-header css-header-apps">
                            <div
                                onClick={(e) => handleFlowClick(flow)}
                                className="css-card-text css-card-apps"
                            >
                                <div
                                    style={{
                                        marginRight: "10px",
                                        display: "flex",
                                        alignItems: "center",
                                    }}
                                >
                                    <div className="mx_DecoratedRoomAvatar mx_DecoratedRoomAvatar_cutout">
                                        <img
                                            className="mx_BaseAvatar_image"
                                            src={require("../../../res/img/element-icons/algorithm.svg")}
                                            alt=""
                                            aria-hidden="true"
                                            style={{
                                                height: "32px",
                                                width: "32px",
                                            }}
                                        />
                                    </div>
                                    <div style={{ marginLeft: "10px" }}>
                                        {flow.public_url && (
                                            <p className="css-title">
                                                {flow.public_url.replace(
                                                    "https://",
                                                    ""
                                                )}
                                            </p>
                                        )}
                                        <p className="css-subtitle">
                                            {flow.is_default}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>
        );
        return details;
    };
    return (
        <React.Fragment>
            <div className="mx_FlowBuilder_container">
                <div className="mx_FlowBuilder_child">
                    <div className="mx_FlowBuilder_child_title">
                        <h1>Your Flows ({flows.length} flows)</h1>
                        <span title="This feature is available for paid plans">
                            <a target="_blank" href="https://tekos.co/pricing/">
                                Premium
                            </a>
                        </span>
                    </div>
                    <p>
                        Edit templates &#38; pre-build applications. Create your
                        own bots, apps &#38; automations without coding.
                    </p>
                    {flows.length > 0 ? _getDetails() : _getButtonCreate()}
                    {SdkConfig.get().flow_api_settings?.api_route_endpoint && (
                        <ApplicationsFlowBuilder
                            endpoint={
                                SdkConfig.get().flow_api_settings
                                    .api_route_endpoint
                            }
                            customer_id={
                                SdkConfig.get().flow_api_settings.customer_id
                            }
                            type="apps"
                            showTop={false}
                            flows={flows}
                        />
                    )}
                </div>
            </div>
        </React.Fragment>
    );
}
