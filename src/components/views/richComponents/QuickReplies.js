"use strict";
// JIRA  :  #MIG-24
import React from "react";
import PropTypes from "prop-types";
import { MatrixClientPeg } from "../../../MatrixClientPeg";

import * as HtmlUtils from "../../../HtmlUtils";
import axios from "axios";
//import { extract } from "../../../tekos/utils/_ApiUtils";
import * as sdk from "../../../index";

//import Modal from "../../../Modal";
export default class QuickReplies extends React.Component {
    static propTypes = {
        mxEvent: PropTypes.object.isRequired, // event with hints
        hint: PropTypes.any,
        index: PropTypes.any,
    };

    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
        this.state = {
            busy: false,
            enabled: true,
            astyle: {
                textDecoration: "none",
                color: "#027be3",
            },
        };
    }

    onClick() {
        let txtToSend, msgType;
        const client = MatrixClientPeg.get();
        txtToSend = this.props.hint.reply
            ? this.props.hint.reply
            : this.props.hint.body;
        msgType = this.props.hint.replynotify ? "m.notice" : "m.text";
        if (this.props.hint.type == "postback") {
            client.sendMessage(this.props.mxEvent.getRoomId(), {
                body: txtToSend,
                msgtype: msgType,
            });
        } else if (this.props.hint.type == "url") {
            let url = this.props.hint.request_url || this.props.hint.value;
            window.open(url, "_blank");
        } else if (this.props.hint.type == "adaptivecard") {
            client.sendMessage(this.props.mxEvent.getRoomId(), {
                body: "",
                msgtype: msgType,
                adaptiveCard: txtToSend,
            });
        } else if (
            this.props.hint.type == "request_url_only" &&
            this.state.enabled
        ) {
            this.setState({ busy: true });
            let posturl = this.props.hint.request_url || this.props.hint.value;
            var header = { headers: { "Content-Type": "application/json" } };

            const data = {
                id: this.props.hint.body,
                msg: txtToSend,
                senderid: client.getUserId(),
                roomid: this.props.mxEvent.getRoomId(),
            };
            axios
                .post(posturl, data, header)
                .then((res) => {
                    this.setState({
                        busy: false,
                        enabled: false,
                        astyle: {
                            textDecoration: "none",
                            color: "gray",
                            border: "1px solid gray",
                            cursor: "not-allowed",
                        },
                    });
                })
                .catch((error) => {
                    console.error("Error in quickreplies", error);
                    this.setState({
                        busy: false,
                        enabled: false,
                        astyle: {
                            textDecoration: "none",
                            color: "gray",
                            border: "1px solid gray",
                            cursor: "not-allowed",
                        },
                    });
                });
        }
    }

    render() {
        const hint = this.props.hint;
        let body;

        if (hint.formatted_body) {
            body = HtmlUtils.bodyToHtml(hint);
        } else if (hint.body) {
            body = hint.body;
        }

        const Spinner = sdk.getComponent("elements.Spinner");
        let spinn;
        spinn = (
            <div>
                <Spinner wait={true} />
            </div>
        );

        return (
            <div
                className="mx_HintButton"
                style={this.state.astyle}
                onClick={this.onClick}
                key={this.props.index}
            >
                {this.state.busy ? spinn : body}
            </div>
        );
    }
}
