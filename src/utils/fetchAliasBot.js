import SdkConfig from "../SdkConfig";
import { MatrixClientPeg } from "../MatrixClientPeg";

export async function fetchBotMenu(state) {
    let matrixEndpoint = SdkConfig.get().default_server_config["m.homeserver"]
        .base_url;

    let url = `${matrixEndpoint}/_matrix/client/r0/sync?access_token=${MatrixClientPeg.get().getAccessToken()}`;

    const response = await fetch(url).then((res) => res.json());
    return response;
}
