import React from "react";
import AccessibleTooltipButton from "../elements/AccessibleTooltipButton";

function RecordAudioButton(props) {
    const onHangupClick = () => {};

    return (
        <AccessibleTooltipButton
            className="mx_MessageComposer_button mx_MessageComposer_recordAudio"
            title="Record audio message"
            disabled={false}
            onClick={props.showAudioRecorder}
        />
    );
}

export default RecordAudioButton;
