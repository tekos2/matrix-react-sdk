import React from "react";
import SdkConfig from "../../SdkConfig";
import loadable from "@loadable/component";
import { MatrixClientPeg } from "../../MatrixClientPeg";

const AmazonFrame = loadable(() => import("./ExternalFrame"), {
    fallback: <div>Loading...</div>,
});

export default function ContactManager(props) {
    const loadIframe = () => {
        const managerSettings = SdkConfig.get()["ContactManagerSettings"];

        if (
            managerSettings &&
            managerSettings?.crm_default_link &&
            managerSettings?.widget_default_link
        ) {
            let link = "https://tekos.co";
            if (props.page == "crm") {
                const client = MatrixClientPeg.get();

                link = managerSettings.crm_default_link;
                if (client.baseUrl) {
                    link += `/?hs=${client.baseUrl}#/`;
                }
            }
            if (props.page == "widget")
                link = managerSettings.widget_default_link;
            return <AmazonFrame src={link} width="100%" height="100%" />;
        } else {
            return (
                <div className="mx_contactManager_container">
                    <p>
                        <b>crm_default_link or widget_default_link</b> env
                        variables must be enabled before accessing /crm
                    </p>
                </div>
            );
        }
    };
    return <React.Fragment>{loadIframe()}</React.Fragment>;
}
