import React from "react";

export default class setReinstallAppDialog extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
        };
    }
    render() {
        const BaseDialog = sdk.getComponent("views.dialogs.BaseDialog");
        const Loader = sdk.getComponent("elements.Spinner");
        let spinner;
        if (this.state.loading) {
            spinner = <Loader />;
        }
        return (
            <BaseDialog
                className="mx_SetEmailDialog"
                onFinished={this.props.onFinished}
                title={this.props.title}
                contentId="mx_Dialog_content"
            >
                <div className="mx_adaptiveCard_modal">
                    <p>hello world</p>
                </div>
            </BaseDialog>
        );
    }
}
