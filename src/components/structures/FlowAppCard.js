import React from "react";
import { MatrixClientPeg } from "../../MatrixClientPeg";
import {
    Kind as SetupKind,
    showToast as showSetupEncryptionToast,
} from "../../toasts/SetupEncryptionToast";
import ReactHtmlParser from "react-html-parser";
import ReactMarkdown from "react-markdown";
import { eventTrack } from "../../utils/eventGenerate";
import * as sdk from "../../index";
import Modal from "../../Modal";

var handleError = function (err) {
    return new Response(
        JSON.stringify({
            code: 400,
            message: "network Error",
        })
    );
};
export default class FlowAppCard extends React.Component {
    constructor() {
        super();
        this.refsArray = [];
        this.state = {
            installing: false,
        };
    }
    async handleClick(card) {
        this.setState({ installing: true });

        const cli = MatrixClientPeg.get();
        let userId = cli.getUserId();
        console.log("start installing");

        let posturl = this.props.endpoint;
        console.log(posturl);
        if (posturl) {
            const flowdata = {
                user_id: userId,
                app_id: card.id,
                flow_json_link: card.json,
                customer_id: this.props.customer_id,
            };
            try {
                let _posturl = posturl + "/api/v1/applications/install";
                const response = await fetch(_posturl, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify(flowdata),
                }).catch(handleError);

                const json = await response.json();
                console.warn(json);
                if (json.response) {
                    if (json.busy) {
                        showSetupEncryptionToast(
                            SetupKind.INSTALLATION_APP_BUSY
                        );
                    } else {
                        eventTrack("app_installed", {
                            app_id: flowdata.app_id,
                            user_id: flowdata.user_id,
                            app_name: card.app_name,
                        });
                        showSetupEncryptionToast(
                            SetupKind.INSTALLATION_APP_SUCCESS
                        );
                    }

                    this.setState({ installing: false });
                } else {
                    this.setState({ installing: false });

                    showSetupEncryptionToast(SetupKind.ERROR_HAS_OCCURED);
                }
            } catch (e) {
                this.setState({ installing: false });

                console.error("an error has occured", e);
            }
        }
    }
    handleReinstallClick(card) {
        console.log("launch reinstall");
        const InstallAppsDialogs = sdk.getComponent(
            "views.dialogs.InstallAppsDialogs"
        );
        Modal.createTrackedDialog(
            "Incoming Verification",
            "",
            InstallAppsDialogs,
            {
                verifier: true,
                install: () => this.handleClick(card),
            },
            null,
            /* priority = */ false,
            /* static = */ true
        );
    }
    handleClickBody(url) {
        if (url) {
            window.open(url, "_blank");
        }
    }
    render() {
        const card = this.props.card;
        if (!card) return <></>;
        let actionbutton;
        let reinstallButton;
        if (this.props.type == "apps") {
            actionbutton = (
                <button
                    onClick={() => this.handleClick(card)}
                    className=" css-add-button-app"
                    disabled={
                        !card.cta_button_enabled ||
                        !this.props.isThereFlows ||
                        this.state.installing
                    }
                >
                    {this.state.installing
                        ? "Installing ..."
                        : card.cta_button_label}
                </button>
            );
        }
        if (card.cta_app_installed) {
            reinstallButton = (
                <button
                    onClick={() => this.handleReinstallClick(card)}
                    className="css-add-button-app-reninstall"
                >
                    Re-install
                </button>
            );
        }

        let bodyandtitle = (
            <div
                onClick={() => this.handleClickBody(card.url)}
                className="css-card-text css-card-apps"
            >
                <div style={{ marginRight: "10px" }}>
                    <img
                        src={card.logo_link}
                        style={{
                            width: "50px",
                            heigh: "50px",
                            borderRadius: "50%",
                        }}
                    />
                </div>
                <div>
                    <p className="css-title">
                        {ReactHtmlParser(
                            card.app_name || card.short_description
                        )}
                    </p>
                    {/*  {ReactHtmlParser(card.long_description)} */}
                    <ReactMarkdown className="css-subtitle">
                        {card.long_description}
                    </ReactMarkdown>
                </div>
            </div>
        );

        return (
            <div
                className="css-header css-header-apps"
                ref={(divElement) => (this.divElement = divElement)}
            >
                {bodyandtitle}
                <div style={{ display: "flex", flexDirection: "column" }}>
                    {actionbutton}
                    {reinstallButton}
                </div>
            </div>
        );
    }
}
