// JIRA : #MIG-9
// Adaptive Card body
import React from "react";
import PropTypes from "prop-types";
import AdaptiveCard from "tekos-adaptivecards-v2";
import axios from "axios";
import { MatrixClientPeg } from "../../../MatrixClientPeg";
import * as sdk from "../../../index";
import Modal from "../../../Modal";
import {
    _openCloudinaryWidget,
    _openStripeSession,
} from "./LoaderAdaptiveCards";
export default class AdaptiveCards extends React.Component {
    static propTypes = {
        mxEvent: PropTypes.object.isRequired, // event with hints
        code: PropTypes.any,
        update: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.onActionSubmitAdaptive = this.onActionSubmitAdaptive.bind(this);
        this.state = {
            active: false,
            code: this.props.code,
        };
    }

    onActionSubmitAdaptive(e) {
        if (e._processedData.url) {
            let data = e._processedData;
            data.roomId = this.props.mxEvent.getRoomId();
            const client = MatrixClientPeg.get();
            data.senderid = client.getUserId();
            let header = { "Content-Type": "application/json" };

            axios
                .post(e._processedData.url, data, header)
                .then((response) => {
                    let reponseAdaptiveCard = response.data;
                    if (
                        reponseAdaptiveCard.$schema &&
                        reponseAdaptiveCard.body
                    ) {
                        // const resultAdaptiveCode = this.affectTrigguerId(data);
                        if (e._processedData.response_action) {
                            if (e._processedData.response_action === "modal") {
                                const client = MatrixClientPeg.get();
                                const SetAdaptiveCardDialog = sdk.getComponent(
                                    "dialogs.SetAdaptiveCardDialog"
                                );

                                Modal.createTrackedDialog(
                                    "Start DM",
                                    e._processedData.url,
                                    SetAdaptiveCardDialog,
                                    {
                                        endpoint: false,
                                        value: reponseAdaptiveCard,
                                        roomId: this.props.mxEvent.getRoomId(),
                                        senderId: client.getUserId(),
                                        onFinished: (inviteIds) => {},
                                    }
                                );
                            }
                            if (
                                e._processedData.response_action === "new_view"
                            ) {
                                //    this.setState({ code: reponseAdaptiveCard });
                            }
                        }
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        } else if (e._processedData["stripe_checkout"]) {
            _openStripeSession(e._processedData["stripe_checkout"]);
        } else if (e._processedData.hintcloud) {
            _openCloudinaryWidget(e._processedData.hintcloud);
        }
    }

    render() {
        if (this.state.code) {
            const card = (
                <AdaptiveCard
                    onActionSubmit={(e) => this.onActionSubmitAdaptive(e)}
                    style={{
                        maxWidth: "450px",
                        borderRadius: "10px",
                    }}
                    //payload={resultAdaptiveCode}
                    payload={this.state.code}
                />
            );

            return <div>{card}</div>;
        } else {
            return <div></div>;
        }
    }
}
