/*
Copyright 2015, 2016 OpenMarket Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
import Field from "../views/elements/Field";
import SdkConfig from "../../SdkConfig";

import React from "react";
import { _t } from "../../languageHandler";
import * as sdk from "../../index";

export default class AddShortCutToolbar extends React.Component {
    constructor() {
        super();
        this.refsArray = [];
        this.state = {
            // use compact timeline view

            emails: [],
            url: "",
            text: "",
        };
    }

    componentWillMount() {
        if (this.props.item) {
            let item = this.props.item;
            this.setState({ url: item.url, text: item.text });
        }
    }
    _deleteOneProfile = () => {
        let url =
            SdkConfig.get().default_server_config["m.homeserver"].base_url +
            "/_matrix/client/r0/rooms/" +
            this.props.aliasroom +
            "/state/m.shortcut?access_token=" +
            SdkConfig.get().master_bot.access_token;
        let oneshortcut = this.props.item;
        oneshortcut.text = null;
        oneshortcut.url = null;
        oneshortcut.enabled = false;
        let allshortcuts = this.props.shortcuts;
        for (var i = 0; i < allshortcuts.length; i++) {
            if (allshortcuts[i].id === oneshortcut.id) {
                allshortcuts[i] = oneshortcut;
                break;
            }
        }
        let data = {
            value: allshortcuts,
        };
        fetch(url, {
            method: "put",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(data),
        })
            .then((response) => {
                response.json().then((data) => {
                    this.props.closeModal();
                });
            })
            .catch((error) => {
                this.props.closeModal();
            });
    };
    _updateProfile = () => {
        if (!this.state.url || !this.state.text) {
            this.props.closeModal();
        } else {
            let url =
                SdkConfig.get().default_server_config["m.homeserver"].base_url +
                "/_matrix/client/r0/rooms/" +
                this.props.aliasroom +
                "/state/m.shortcut?access_token=" +
                SdkConfig.get().master_bot.access_token;
            let oneshortcut = this.props.item;
            oneshortcut.text = this.state.text;
            oneshortcut.url = this.state.url;
            oneshortcut.enabled = true;
            let allshortcuts = this.props.shortcuts;
            for (var i = 0; i < allshortcuts.length; i++) {
                if (allshortcuts[i].id === oneshortcut.id) {
                    allshortcuts[i] = oneshortcut;
                    break;
                }
            }
            let data = {
                value: allshortcuts,
            };
            fetch(url, {
                method: "put",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(data),
            })
                .then((response) => {
                    response.json().then((data) => {
                        this.props.closeModal();
                    });
                })
                .catch((error) => {
                    this.props.closeModal();
                });
        }
    };
    _saveProfile = () => {
        if (!this.state.url || !this.state.text) {
            this.props.closeModal();
        } else {
            let url =
                SdkConfig.get().default_server_config["m.homeserver"].base_url +
                "/_matrix/client/r0/rooms/" +
                this.props.aliasroom +
                "/state/m.shortcut?access_token=" +
                SdkConfig.get().master_bot.access_token;
            let oneshortcut = {
                id: this.props.shortcuts.length + 1,
                url: this.state.url,
                text: this.state.text,
                enabled: true,
            };
            let allshortcuts = this.props.shortcuts;
            allshortcuts.push(oneshortcut);
            let data = {
                value: allshortcuts,
            };
            fetch(url, {
                method: "put",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(data),
            })
                .then((response) => {
                    response.json().then((data) => {
                        this.props.closeModal();
                    });
                })
                .catch((error) => {
                    this.props.closeModal();
                });
        }
    };
    _onUrlChanged = (e) => {
        this.setState({ url: e.target.value });
    };
    _onTextChanged = (e) => {
        this.setState({ text: e.target.value });
    };
    render() {
        const AccessibleButton = sdk.getComponent("elements.AccessibleButton");

        return (
            <React.Fragment>
                {!this.props.edit ? (
                    <h1>Add Shortcut</h1>
                ) : (
                    <h1>Update Shortcut</h1>
                )}

                <Field
                    id="profileDisplayName"
                    label="Shortcut url"
                    type="text"
                    value={this.state.url}
                    autoComplete="off"
                    onChange={this._onUrlChanged}
                />
                <Field
                    id="profileDisplayName"
                    label="Shortcut label"
                    type="text"
                    value={this.state.text}
                    autoComplete="off"
                    onChange={this._onTextChanged}
                />
                {!this.props.edit ? (
                    <AccessibleButton
                        onClick={this._saveProfile}
                        kind="primary"
                    >
                        {_t("Save")}
                    </AccessibleButton>
                ) : (
                    <div
                        style={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "space-between",
                            alignItems: "baseline",
                        }}
                    >
                        <AccessibleButton
                            onClick={this._updateProfile}
                            kind="primary"
                        >
                            Update
                        </AccessibleButton>
                        <AccessibleButton onClick={this._deleteOneProfile}>
                            Delete
                        </AccessibleButton>
                    </div>
                )}
            </React.Fragment>
        );
    }
}
