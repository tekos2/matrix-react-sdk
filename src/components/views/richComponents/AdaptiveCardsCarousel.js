// JIRA : #MIG-9
// Carousel for adaptive cards
import React, { useState } from "react";
import Carousel from "react-elastic-carousel";
import AdaptiveCard from "tekos-adaptivecards-v2";
import axios from "axios";
import { MatrixClientPeg } from "../../../MatrixClientPeg";
import * as sdk from "../../../index";
import Modal from "../../../Modal";
import {
    _openCloudinaryWidget,
    _openStripeSession,
} from "./LoaderAdaptiveCards";

export default function AdaptiveCardsCarousel(props) {
    // breakpoints used to adjust the responsivness of the carousel
    let [breakPoints, setBreakPoints] = useState([
        { width: 1, itemsToShow: 1 },
        { width: 550, itemsToShow: 2, itemsToScroll: 2, pagination: false },
        { width: 850, itemsToShow: 3 },
        { width: 1150, itemsToShow: 4, itemsToScroll: 2 },
        { width: 1450, itemsToShow: 5 },
        { width: 1750, itemsToShow: 6 },
    ]);
    const onActionSubmitAdaptive = (e) => {
        if (e._processedData.url) {
            let data = e._processedData;
            data.roomId = this.props.mxEvent.getRoomId();
            const client = MatrixClientPeg.get();
            data.senderid = client.getUserId();
            let header = { "Content-Type": "application/json" };

            axios
                .post(e._processedData.url, data, header)
                .then((response) => {
                    let reponseAdaptiveCard = response.data;
                    if (
                        reponseAdaptiveCard.$schema &&
                        reponseAdaptiveCard.body
                    ) {
                        // const resultAdaptiveCode = this.affectTrigguerId(data);
                        if (e._processedData.response_action) {
                            if (e._processedData.response_action === "modal") {
                                const client = MatrixClientPeg.get();
                                const SetAdaptiveCardDialog = sdk.getComponent(
                                    "dialogs.SetAdaptiveCardDialog"
                                );

                                Modal.createTrackedDialog(
                                    "Start DM",
                                    e._processedData.url,
                                    SetAdaptiveCardDialog,
                                    {
                                        endpoint: false,
                                        value: reponseAdaptiveCard,
                                        roomId: this.props.mxEvent.getRoomId(),
                                        senderId: client.getUserId(),
                                        onFinished: (inviteIds) => {},
                                    }
                                );
                            }
                            /*  if (
                                e._processedData.response_action === "new_view"
                            ) {
                                this.setState({ code: LoaderAdaptiveCards() });
                            } */
                        }
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        } else if (e._processedData["stripe_checkout"]) {
            _openStripeSession(e._processedData["stripe_checkout"]);
        } else if (e._processedData.hintcloud) {
            _openCloudinaryWidget(e._processedData.hintcloud);
        }
    };
    const GenerateAdaptiveCards = (cards) => {
        let _cards;
        _cards = cards.map((item, index) => {
            return (
                <div key={index}>
                    <AdaptiveCard
                        key={index}
                        style={{
                            maxWidth: "450px",
                            borderRadius: "10px",
                        }}
                        payload={item}
                        onActionSubmit={onActionSubmitAdaptive}
                    />
                </div>
            );
        });
        return _cards;
    };
    return (
        <Carousel breakPoints={breakPoints}>
            {GenerateAdaptiveCards(props.cards)}
        </Carousel>
    );
}
