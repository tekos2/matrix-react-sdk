import React from "react";

export default function AmazonFrame({ src, width, height }) {
    return (
        <iframe
            frameBorder="0"
            src={src}
            width={width}
            height={height}
        ></iframe>
    );
}
