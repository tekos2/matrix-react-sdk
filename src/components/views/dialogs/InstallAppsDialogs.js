import React, { Component } from "react";
import * as sdk from "../../../index";
import { _t } from "../../../languageHandler";

export default class InstallAppsDialogs extends Component {
    constructor() {
        super();
        this.state = {
            proceed: false,
        };
    }
    _onFinished = () => {
        this.props.onFinished(true);
    };
    _onAcknowledgeClick = () => {
        this.props.onFinished();
    };

    _onOpenSettingsClick = async () => {
        // this.props.onFinished();
        this.setState({ proceed: true });
        await this.props.install();
        this.props.onFinished();
    };

    render() {
        const BaseDialog = sdk.getComponent("dialogs.BaseDialog");
        const DialogButtons = sdk.getComponent("views.elements.DialogButtons");

        return (
            <BaseDialog
                title="Are you sure to perform this action ?"
                onFinished={this._onFinished}
                fixedWidth={false}
            >
                <p style={{ maxWidth: "650px" }}>
                    As this app is previously added in your Flow, you need to
                    follow the deinstallation steps first before re-installing
                    the app, if it's not already done.
                </p>
                <DialogButtons
                    primaryButton={_t("I want to take the risk")}
                    onPrimaryButtonClick={this._onOpenSettingsClick}
                    cancelButton={_t("Cancel")}
                    onCancel={this._onAcknowledgeClick}
                    primaryDisabled={this.state.proceed}
                    spinner={this.state.proceed}
                />
            </BaseDialog>
        );
    }
}
