import * as rudderanalytics from "rudder-sdk-js";
export function rudderInitialize() {
    rudderanalytics.ready(() => {
        console.log("we are all set!!!");
    });
    rudderanalytics.load(
        "1myJsTYlZnZUeewzZiD4fnxguCJ",
        "https://hosted.rudderlabs.com",
        {
            logLevel: "NONE",
            integrations: { All: true },
        }
    );
}

export function eventIdentify(userid, data) {
    // var rudderanalytics = global.window.rudderanalytics = global.window.rudderanalytics || [];
    //  var analytics = (global.window.analytics = global.window.analytics || []);
    rudderanalytics.identify(userid, data, { integrations: { All: true } });
    // rudderanalytics.identify("Simple_User_identify",{},{},{integrations:{All:true}});
}

export function eventPage() {
    rudderanalytics.page(
        "Cart",
        "Cart Viewed",
        {
            path: "xcx123",
            referrer: "xc dd",
            search: "sefsefes",
            title: "edwewde",
            url: "wedwedewdew",
        },
        {
            context: {
                ip: "0.0.0.0",
            },
            anonymousId: "xx",
        },
        () => {
            console.log("in page call");
        }
    );
}

export function eventTrack(trackid, data) {
    rudderanalytics.track(trackid, data);
}
