//JIRA : #MIG-10
// description : create class for props bot shortcuts

import React from "react";
import { _t, _td } from "../../../languageHandler";
import { MatrixClientPeg } from "../../../MatrixClientPeg";
import axios from "axios";
import { ContextMenu, MenuItem } from "../../structures/ContextMenu";
import AccessibleTooltipButton from "../elements/AccessibleTooltipButton";
import * as sdk from "../../../index";
import Modal from "../../../Modal";

const STICKERPICKER_Z_INDEX = 3500;

// Key to store the widget's AppTile under in PersistedElement
export default class MenuPicker extends React.Component {
    constructor(props) {
        super(props);
        this._onShowStickersClick = this._onShowStickersClick.bind(this);
        this._onHideStickersClick = this._onHideStickersClick.bind(this);
        this._onFinished = this._onFinished.bind(this);
        this._onAction = this._onAction.bind(this);
        this.popoverWidth = 300;
        this.popoverHeight = 300;

        // This is loaded by _acquireScalarClient on an as-needed basis.
        this.scalarClient = null;
        this.state = {
            showStickers: false,
            imError: null,
            stickerpickerX: null,
            stickerpickerY: null,
            stickerpickerWidget: null,
            widgetId: null,
        };
    }

    componentWillUnmount() {}

    _onHideStickersClick = (ev) => {
        this.setState({ showStickers: false });
    };
    _onShowStickersClick = (e) => {
        const buttonRect = e.target.getBoundingClientRect();

        // The window X and Y offsets are to adjust position when zoomed in to page
        let x = buttonRect.right + window.pageXOffset - 41;
        const rightPad = 10;

        // When the sticker picker would be displayed off of the viewport, adjust x
        //  (302 = width of context menu, including borders)
        x = Math.min(x, document.body.clientWidth - (302 + rightPad));

        // Offset the chevron location, which is relative to the left of the context menu
        //  (10 = offset when context menu would not be displayed off viewport)
        //  (2 = context menu borders)
        const stickerPickerChevronOffset = Math.max(
            10,
            2 + window.pageXOffset + buttonRect.left - x
        );

        const y =
            buttonRect.top + buttonRect.height / 2 + window.pageYOffset - 19;

        this.setState({
            showStickers: true,
            stickerPickerX: x,
            stickerPickerY: y,
            stickerPickerChevronOffset,
        });
    };
    /**
     * Trigger hiding of the sticker picker overlay
     * @param  {Event} ev Event that triggered the function call
     */
    _onHideStickersClick(ev) {
        this.setState({ showStickers: false });
    }
    /**
     * The stickers picker was hidden
     */
    _onFinished = () => {
        this.setState({ showStickers: false });
    };
    _onAction = (item) => {
        console.log(item);
        const client = MatrixClientPeg.get();

        if (item.type === "postback") {
            client.sendMessage(this.props.roomId, {
                body: item.value,
                msgtype: "m.text",
            });
        } else if (item.type === "url") {
            let url = item.value;
            window.open(url, "_blank");
        } else if (
            item.type === "post_request" ||
            item.type === "AdaptiveCard"
        ) {
            let posturl = item.value;
            var header = { headers: { "Content-Type": "application/json" } };

            const data = {
                msg: item.body,
                senderid: client.getUserId(),
                roomid: this.props.roomId,
            };
            console.log(posturl);
            axios
                .post(posturl, data, header)
                .then((res) => {
                    console.warn(res);
                    if (res.data?.$schema) {
                        const SetAdaptiveCardDialog = sdk.getComponent(
                            "dialogs.SetAdaptiveCardDialog"
                        );
                        const reponseAdaptiveCard = res.data;
                        Modal.createTrackedDialog(
                            "Start DM",
                            posturl,
                            SetAdaptiveCardDialog,
                            {
                                endpoint: false,
                                value: reponseAdaptiveCard,
                                roomId: this.props.roomId,
                                senderId: client.getUserId(),
                                onFinished: (inviteIds) => {},
                            }
                        );
                    }
                })
                .catch((error) => {
                    console.error("Error in quickreplies", error);
                });
        }
        this.setState({ showStickers: false });
    };
    _loadMenuItemsFromBot = (menu) => {
        let _menuItems;
        _menuItems = menu.map((item, index) => {
            return (
                <MenuItem
                    key={index}
                    className="mx_MessageContextMenu_field mx_MessageContextMenu_field_bot_menu"
                    onClick={() => this._onAction(item)}
                >
                    {item.body}
                </MenuItem>
            );
        });
        return _menuItems;
    };
    _loadStateMenuButtons = () => {
        let _menuBots;
        let _arraybots = [];
        const _botmenu = this.props.botmenu;
        for (var key of Object.keys(_botmenu)) {
            _arraybots.push({
                boid: key,
                data: _botmenu[key],
            });
        }
        _menuBots = _arraybots.map((item, index) => {
            let icon =
                item.data[0].content.icon ||
                "https://tekosassets.s3.eu-central-1.amazonaws.com/images/robot.svg";
            return (
                <React.Fragment key={index}>
                    <div className="mx_MessageContextMenu_field_host">
                        <div className="mx_MessageContextMenu_field_container">
                            <img
                                style={{ width: "25px", height: "25px" }}
                                src={icon}
                            />
                            <MenuItem className="mx_MessageContextMenu_field mx_MessageContextMenu_field_bot_icon ">
                                {item.boid}
                            </MenuItem>
                        </div>

                        {this._loadMenuItemsFromBot(item.data[0].content.menu)}
                    </div>
                    <div
                        style={{ borderTop: "1px solid rgb(214, 214, 214)" }}
                    ></div>
                </React.Fragment>
            );
        });
        return <React.Fragment>{_menuBots}</React.Fragment>;
    };

    render() {
        let stickerPicker;
        let stickersButton;
        if (this.state.showStickers) {
            stickersButton = (
                <AccessibleTooltipButton
                    className="mx_MessageComposer_button mx_MessageComposer_menu"
                    onClick={this._onHideStickersClick}
                    key="controls_hide_stickers"
                    title={_t("Open Bot Shortcut")}
                ></AccessibleTooltipButton>
            );
            stickerPicker = (
                <ContextMenu
                    chevronOffset={this.state.stickerPickerChevronOffset}
                    chevronFace="bottom"
                    scroll={true}
                    left={this.state.stickerPickerX}
                    top={this.state.stickerPickerY}
                    menuWidth={this.popoverWidth}
                    onFinished={this._onFinished}
                    menuPaddingTop={0}
                    menuPaddingLeft={0}
                    menuPaddingRight={0}
                    zIndex={STICKERPICKER_Z_INDEX}
                >
                    {this._loadStateMenuButtons()}
                </ContextMenu>
            );
        } else {
            stickersButton = (
                <AccessibleTooltipButton
                    className="mx_MessageComposer_button mx_MessageComposer_menu"
                    onClick={this._onShowStickersClick}
                    title={_t("Open Bot Shortcut")}
                    key="controls_show_stickers"
                ></AccessibleTooltipButton>
            );
        }

        return (
            <React.Fragment>
                {stickersButton}
                {stickerPicker}
            </React.Fragment>
        );
    }
}
