import React, { useEffect, Fragment, useState } from "react";
import ReactMarkdown from "react-markdown";

const TopBarManager = (props) => {
    const [owner, setOwner] = useState(false);
    const [text, setText] = useState("");
    useEffect(() => {
        // Create an scoped async function in the hook

        async function fetchConnectedUser() {
            try {
                const flowApiSettings = props.flow_api_settings;

                const response = await fetch(
                    `${flowApiSettings.api_route_endpoint}/api/v1/workspace_top_bar/${flowApiSettings.customer_id}/${props.userId}`
                );
                const data = await response.json();
                if (data.owner && data.message) {
                    setText(data.message);
                    setOwner(true);
                }
                console.warn(data);
            } catch (ex) {
                console.error(ex);
            }
        }
        fetchConnectedUser();
    }, []);
    const closeBar = () => {
        setOwner(false);
    };

    if (!owner) return <Fragment></Fragment>;
    return (
        <div className="mx_topBarManager">
            <ReactMarkdown>{text}</ReactMarkdown>
            <i
                aria-label="icon: close"
                tabindex="-1"
                className="mx_anticon"
                style={{
                    position: "absolute",
                    right: "20px",
                    cursor: "pointer",
                }}
                onClick={closeBar}
            >
                <svg
                    viewBox="64 64 896 896"
                    focusable="false"
                    data-icon="close"
                    width="1em"
                    height="1em"
                    fill="currentColor"
                    aria-hidden="true"
                >
                    <path d="M563.8 512l262.5-312.9c4.4-5.2.7-13.1-6.1-13.1h-79.8c-4.7 0-9.2 2.1-12.3 5.7L511.6 449.8 295.1 191.7c-3-3.6-7.5-5.7-12.3-5.7H203c-6.8 0-10.5 7.9-6.1 13.1L459.4 512 196.9 824.9A7.95 7.95 0 0 0 203 838h79.8c4.7 0 9.2-2.1 12.3-5.7l216.5-258.1 216.5 258.1c3 3.6 7.5 5.7 12.3 5.7h79.8c6.8 0 10.5-7.9 6.1-13.1L563.8 512z"></path>
                </svg>
            </i>
        </div>
    );
};
export default TopBarManager;
