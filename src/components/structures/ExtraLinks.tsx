/*
Copyright 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import * as React from "react";
import { MatrixClientPeg } from "../../MatrixClientPeg";

import { _t } from "../../languageHandler";

export default class ExtraLinks extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            query: "",
            focused: false,
            isSynapseAdmin: false,
        };
    }
    componentWillMount() {
        const client = MatrixClientPeg.get();
        client.isSynapseAdministrator().then(
            (isAdmin) => {
                this.setState({ isSynapseAdmin: isAdmin });
            },
            () => {
                this.setState({ isSynapseAdmin: false });
            }
        );
    }
    private onAction(type: String) {
        console.log(type);
        window.location = `/#/${type}`;
    }

    public render(): React.ReactNode {
        return (
            <div className="mx_RoomSublist_tiles mx_extra_links">
                <div
                    role="treeitem"
                    aria-label="Flow Builder"
                    aria-selected="false"
                    className="mx_AccessibleButton mx_RoomTile"
                    style={{ marginBottom: "0" }}
                    onClick={() => this.onAction("apps")}
                >
                    <div className="mx_DecoratedRoomAvatar mx_DecoratedRoomAvatar_cutout">
                        <img
                            className="mx_BaseAvatar_image"
                            src={require("../../../res/img/element-icons/algorithm.svg")}
                            alt=""
                            aria-hidden="true"
                            style={{ height: "32px", width: "32px" }}
                        />
                    </div>
                    <div className="mx_RoomTile_nameContainer">
                        <div
                            title="Flow Builder"
                            className="mx_RoomTile_name"
                            dir="auto"
                        >
                            Flow Builder &#38; Apps
                        </div>
                    </div>
                </div>
                {this.state.isSynapseAdmin ? (
                    <div
                        role="treeitem"
                        aria-label="Contacts &amp; Users"
                        aria-selected="false"
                        className="mx_AccessibleButton mx_RoomTile"
                        style={{ marginBottom: "0" }}
                        onClick={() => this.onAction("crm")}
                    >
                        <div className="mx_DecoratedRoomAvatar mx_DecoratedRoomAvatar_cutout">
                            <img
                                className="mx_BaseAvatar_image"
                                src={require("../../../res/img/element-icons/phone-book.svg")}
                                alt=""
                                aria-hidden="true"
                                style={{ height: "32px", width: "32px" }}
                            />
                        </div>
                        <div className="mx_RoomTile_nameContainer">
                            <div
                                title=" Contacts &amp; Users"
                                className="mx_RoomTile_name"
                                dir="auto"
                            >
                                Contacts &amp; Users
                            </div>
                        </div>
                    </div>
                ) : (
                    <div
                        role="treeitem"
                        aria-label="Contacts &amp; Users"
                        aria-selected="false"
                        className="mx_AccessibleButton mx_RoomTile"
                        style={{ marginBottom: "0" }}
                    ></div>
                )}
               {/*  <div
                    role="treeitem"
                    aria-label="Live Chat Widget"
                    aria-selected="false"
                    className="mx_AccessibleButton mx_RoomTile"
                    style={{ marginBottom: "0" }}
                    onClick={() => this.onAction("widgets")}
                >
                    <div className="mx_DecoratedRoomAvatar mx_DecoratedRoomAvatar_cutout">
                        <img
                            className="mx_BaseAvatar_image"
                            src={require("../../../res/img/element-icons/blocks.svg")}
                            alt=""
                            aria-hidden="true"
                            style={{ height: "32px", width: "32px" }}
                        />
                    </div>
                    <div className="mx_RoomTile_nameContainer">
                        <div
                            title="Live Chat Widget"
                            className="mx_RoomTile_name"
                            dir="auto"
                        >
                            Live Chat Widget
                        </div>
                    </div>
                </div> */}
            </div>
        );
    }
}
