/*
Copyright 2017 Michael Telatynski <7t3chguy@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
JIRA : #CHAT-976
*/

import React, { createRef } from "react";
import * as sdk from "../../index";
import axios from "axios";
import { _t } from "../../languageHandler";
import { MatrixClientPeg } from "../../MatrixClientPeg";
import ReactPaginate from "react-paginate";
import SdkConfig from "../../SdkConfig";
import FlowAppCard from "./FlowAppCard";
import AccessibleButton from "../views/elements/AccessibleButton";

export default class ApplicationsFlowBuilder extends React.Component {
    constructor() {
        super();
        this.refsArray = [];
        this.state = {
            loading: true,
            data: [],
            isdata: false,
            senderId: null,
            email: null,
            whitedata: [],
            categories: [],
            alldata: [],
            searchTerm: "",
            currentPage: 1,
            postsPerPage: 12,
            isThereFlows: false,
        };
    }
    handlePageChange(pageNumber) {
        this.setState({ activePage: pageNumber });
    }
    _roomCreateOptions() {}
    _PaginatePosts(table) {
        const indexOfLastPost =
            this.state.currentPage * this.state.postsPerPage;
        const indexofFirstPage = indexOfLastPost - this.state.postsPerPage;
        const currentsPosts = table.slice(indexofFirstPage, indexOfLastPost);
        return currentsPosts;
    }
    onChange() {
        if (!this._search.current) {
            this.setState({ data: this.state.alldata }, function () {
                return;
            });
        }
        this.setState({ searchTerm: this._search.current.value }, function () {
            this.onSearch();
        });
    }
    componentDidUpdate(prevProps) {
        if (
            prevProps.flows &&
            Array.isArray(prevProps.flows) &&
            prevProps.flows != this.props.flows &&
            this.props.flows.length > 0
        ) {
            this.setState({ isThereFlows: true });
        }
        if (prevProps.type != this.props.type) {
            this.reloadData();
        }
    }
    onSearch() {
        let result;
        let c = this.state.searchTerm.toString().toLowerCase();
        let arraydata = this.state.alldata;
        if (c != "") {
            result = arraydata.filter(function (v) {
                if (v.categories && v.short_description && v.long_description) {
                    return (
                        v.categories.toString().toLowerCase().includes(c) ||
                        v.short_description
                            .toString()
                            .toLowerCase()
                            .includes(c) ||
                        v.long_description.toString().toLowerCase().includes(c)
                    );
                } else if (v.categories && v.short_description) {
                    return (
                        v.categories.toString().toLowerCase().includes(c) ||
                        v.short_description.toString().toLowerCase().includes(c)
                    );
                } else if (v.short_description) {
                    return v.short_description
                        .toString()
                        .toLowerCase()
                        .includes(c);
                }
            });

            this.setState({ data: result });
        } else {
            this.setState({ data: this.state.alldata });
        }
    }
    // this function is used to reload the data once the component is changed from apps to integrations or viceversa
    async reloadData() {
        const cli = MatrixClientPeg.get();
        let userId = cli.getUserId();
        /*  const getUrl = window.location;
        const boturl = "/#/user/" + id;
        window.location.href = boturl; */
        let posturl =
            this.props.endpoint +
            "/api/v1/integrations/details" +
            "/" +
            cli.getUserId();
        if (this.props.type == "apps") {
            posturl = this.props.endpoint + "/api/v1/applications/details";
        }
        const flowApiSettings = SdkConfig.get()["flow_api_settings"];
        var header = { headers: { "Content-Type": "application/json" } };
        if (flowApiSettings?.customer_id) {
            header.headers.customer_id = flowApiSettings.customer_id;
            header.headers.user_id = cli.getUserId();
        }
        if (posturl) {
            const response = await fetch(posturl);
            const json = await response.json();
            if (Array.isArray(json)) {
                //429
                // wait 1 - 2 min before installing a new app

                axios
                    .get(posturl, header)
                    .then((result) => {
                        if (result.data["0"]) {
                            const data = this.FilterWhiteList(result.data);
                            this.setState({ data: data });
                            this.setState({ alldata: data });
                            const allcat = this.FilterCategoriesList(
                                this.state.data
                            );
                            this.setState({ categories: allcat });
                            this.setState({ loading: false });
                        }
                    })
                    .catch((error) => {
                        // mapdata = [];

                        this.setState({
                            loading: false,
                            alldata: [],
                            data: [],
                        });
                    });
            }
        }
    }
    componentDidMount() {
        /*  if (this.props.flows && Array.isArray(this.props.flows)) {
            if (this.props.flows.length > 0) {
                this.setState({ isThereFlows: true });
            }
        } */
        this._search = createRef();

        const cli = MatrixClientPeg.get();

        this.setState({ senderId: cli.getUserId() });
        cli.getThreePids().then((addresses) => {
            let emails = addresses.threepids.filter(
                (a) => a.medium === "email"
            );

            if (emails.length > 0) {
                this.setState({ email: emails[0].address });
            }
        });

        let posturl =
            this.props.endpoint +
            "/api/v1/integrations/details" +
            "/" +
            cli.getUserId();
        if (this.props.type == "apps") {
            posturl = this.props.endpoint + "/api/v1/applications/details";
        }
        const flowApiSettings = SdkConfig.get()["flow_api_settings"];

        var header = { headers: { "Content-Type": "application/json" } };
        if (flowApiSettings?.customer_id) {
            header.headers.customer_id = flowApiSettings.customer_id;
            header.headers.user_id = cli.getUserId();
        }
        axios
            .get(posturl, header)
            .then((result) => {
                if (result.data["0"]) {
                    const data = this.FilterWhiteList(result.data);
                    this.setState({ data: data });
                    this.setState({ alldata: data });
                    const allcat = this.FilterCategoriesList(this.state.data);
                    this.setState({ categories: allcat });
                    this.setState({ loading: false });
                }
            })
            .catch((error) => {
                // mapdata = [];
                this.setState({ loading: false, alldata: [], data: [] });
            });
    }
    componentWillUnmount() {
        // this.setState({ data: [] });
    }

    onCancel() {
        this.props.onFinished(false);
    }
    FilterCategoriesList(apps) {
        let allcategories = [];
        allcategories.push("All");
        for (var i = 0; i < apps.length; i++) {
            if (apps[i].categories) {
                for (let k = 0; k < apps[i].categories.length; k++) {
                    allcategories.push(apps[i].categories[k]);
                }
            }
        }
        allcategories = allcategories.reduce(function (a, b) {
            if (a.indexOf(b) < 0) a.push(b);
            return a;
        }, []);

        return allcategories;
    }
    filterByCategories(c, index) {
        this.setState({ currentPage: 1 }, function () {
            for (let i = 0; i < this.refsArray.length; i++) {
                let e = this.refsArray[i];
                e.removeAttribute("style");
            }
            const element = this.refsArray[index];
            const style = "border-bottom: 3px solid #4f5ff3";

            element.setAttribute("style", style);

            let result;
            let arraydata = this.state.alldata;
            if (c != "All") {
                result = arraydata.filter(function (v) {
                    if (v.categories) return v.categories.includes(c);
                });
            } else {
                result = this.state.alldata;
            }
            this.setState({ data: result });
        });

        //const result = arraydata.filter(data => data.categories.includes(c));
    }
    onExploreCommunityTemplate = () => {
        window.open("https://flows.tekos.co", "_blank");
    };
    FilterWhiteList(apps) {
        for (let i = 0; i < apps.length; i++) {
            let split = false;

            if (apps[i].whitelist && apps[i].visibility == "private") {
                for (var j = 0; j < apps[i].whitelist.length; j++) {
                    if (
                        apps[i].whitelist[j].medium == "email" &&
                        this.state.email
                    ) {
                        let pattern = apps[i].whitelist[j].pattern;
                        pattern = pattern.split("@")[1];
                        let testemail = this.state.email;
                        testemail = testemail.split("@")[1];
                        if (pattern && testemail) {
                            if (pattern != testemail) {
                                split = true;
                            }
                        }
                    }
                    if (
                        apps[i].whitelist[j].medium == "id" &&
                        this.state.senderId
                    ) {
                        let pattern = apps[i].whitelist[j].pattern;
                        const nameid = pattern.split("*")[0];
                        const serverid = pattern.split(":")[1];
                        const senderlogin = this.state.senderId.split(":")[0];
                        const senderserver = this.state.senderId.split(":")[1];

                        if (
                            nameid.indexOf(senderlogin) == -1 ||
                            serverid != senderserver
                        ) {
                            split = true;
                        }
                    }
                }
            }
            if (split) {
                apps.splice(i, 1);
            } else {
                /* if (apps[i].categories) {
                  
                    for (let k = 0; k < apps[i].categories.length; k++) {
                        allcategories.push(apps[i].categories[k]);
                    }
                } */
            }
        }
        /*     if (allcategories.length > 0) {
            allcategories = allcategories.reduce(function(a, b) {
                if (a.indexOf(b) < 0) a.push(b);
                return a;
            }, []);
            this.setState({ categories: allcategories });
        } */
        return apps;
    }
    GetSortOrder(prop) {
        return function (a, b) {
            if (a[prop] > b[prop]) {
                return 1;
            } else if (a[prop] < b[prop]) {
                return -1;
            }
            return 0;
        };
    }
    paginate(num) {
        this.setState({ currentPage: num });
    }

    paginateReact(data) {
        let selected = data.selected + 1;
        this.setState({ currentPage: selected });
    }

    render() {
        const Loader = sdk.getComponent("elements.Spinner");
        //const TagPanelEmbed = sdk.getComponent("structures.TagPanelEmbed");

        let spinner;
        let cards;
        if (this.state.loading) {
            spinner = <Loader />;
        }
        let allappsarray = this.state.data;
        const countKey = Object.keys(allappsarray).length;
        if (countKey > 0) {
            allappsarray.sort(this.GetSortOrder("listing_order"));
            allappsarray = this._PaginatePosts(allappsarray);
            const flowApiSettings = SdkConfig.get()["flow_api_settings"];
            //allappsarray = this.state.data;
            cards = allappsarray.map((card, index) => {
                return (
                    <FlowAppCard
                        isThereFlows={this.state.isThereFlows}
                        card={card}
                        key={index}
                        type="apps"
                        endpoint={this.props.endpoint}
                        customer_id={flowApiSettings.customer_id}
                    />
                );
            });
        }
        let itemcategories;
        let searchbox;
        let pagemodule;

        if (this.state.categories.length > 0) {
            if (this.props.type == "apps") {
                itemcategories = (
                    <div style={{ flex: "1" }}>
                        <div style={{ display: "flex", alignItems: "center" }}>
                            <h1 style={{ padding: "10px 0px" }}>
                                Applications
                            </h1>
                            <span
                                style={{
                                    backgroundColor: "orange",
                                    color: "#fff",
                                    padding: "5px 10px",
                                    marginLeft: " 10px",
                                    borderRadius: "25px",
                                }}
                                title="This feature is still in beta testing"
                            >
                                Beta
                            </span>
                        </div>
                        <div>
                            Ready-to-use templates &#38; apps library. Add, then
                            customize them in your Flow.{" "}
                            <a target="_blank" href="http://bit.ly/apps-doc">
                                Learn more.
                            </a>
                        </div>
                        <AccessibleButton
                            className="mx_MemberList_invite mx_MemberList_explore_community"
                            onClick={this.onExploreCommunityTemplate}
                            style={{
                                margin: "0",
                                marginTop: "25px",
                                maxWidth: "300px",
                            }}
                        >
                            <span>Explore Community Templates</span>
                        </AccessibleButton>
                        <br />
                    </div>
                );
            }
            if (this.props.type != "apps") {
                searchbox = (
                    <div>
                        <input
                            ref={this._search}
                            onChange={this.onChange}
                            className="mx_textinput_icon mx_textinput_search mother_of_all_apps_input"
                            type="text"
                            placeholder={_t(
                                "Search by name or category (e.g. productivity, sales)"
                            )}
                        />
                    </div>
                );
                pagemodule = (
                    <div>
                        <ReactPaginate
                            previousLabel={_t("Previous")}
                            nextLabel={_t("Next")}
                            breakLabel={"..."}
                            breakClassName={"break-me"}
                            pageCount={this.state.data.length / 12}
                            marginPagesDisplayed={2}
                            pageRangeDisplayed={5}
                            onPageChange={this.paginateReact}
                            containerClassName={"pagination"}
                            subContainerClassName={"pages pagination"}
                            activeClassName={"active"}
                        />
                    </div>
                );
            }
        }

        return (
            <React.Fragment>
                {spinner}
                <div className="mx_mother_of_all_apps_container">
                    {itemcategories}
                    <div
                        style={{
                            flex: "4",
                            width: "100%",
                        }}
                    >
                        {this.props.type == "apps" && searchbox}
                        <div className="mother_of_all_apps">{cards}</div>
                        {pagemodule}
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
