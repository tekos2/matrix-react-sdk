/*
Copyright 2017 Vector Creations Ltd
Copyright 2018 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React from "react";
import PropTypes from "prop-types";
import * as sdk from "../../../index";
import { _t } from "../../../languageHandler";
import AdaptiveCard from "tekos-adaptivecards-v2";
import { MatrixClientPeg } from "../../../MatrixClientPeg";
import SdkConfig from "../../../SdkConfig";
import {
    _openCloudinaryWidget,
    _openStripeSession,
} from "../richComponents/LoaderAdaptiveCards";
import axios from "axios";

/**
 * Prompt the user to set an email address.
 *
 * On success, `onFinished(true)` is called.
 */
export default class SetAdaptiveCardDialog extends React.Component {
    static propTypes = {
        onFinished: PropTypes.func.isRequired,
    };
    state = {
        emailAddress: "",
        emailBusy: false,
        adaptivevalue: this.props.value,
        listitems: [],
        noItems: false,
        error: false,
        loading: false,
    };
    onActionSubmitAdaptive(e) {
        let url = e.data.url;
        if (url) {
            let data = e._processedData;
            data.roomId = this.props.roomId;
            data.userId = this.props.senderId;
            if (data.requestUserAccessToken) {
                delete data.requestUserAccessToken;
                data.accessToken = MatrixClientPeg.get().getAccessToken();
            }
            if (this.props.eventId) {
                data.eventId = this.props.eventId;
            }
            let header = { "Content-Type": "application/json" };

            try {
                axios
                    .post(url, data, header)
                    .then((response) => {
                        if (response.status != 200) {
                            this.setState({ error: true });
                            this.props.onFinished();
                        } else {
                            /*  if (e.data.closeafterdata) {
                                if (Array.isArray(data) && data.length > 0) {
                                    this.setState({ noItems: false });

                                    this.setState({ listitems: data });
                                } else {
                                    this.setState({ listitems: [] });
                                    this.setState({ noItems: true });
                                }
                            } else */ if (
                                response.data.$schema
                            ) {
                                this.setState({ adaptivevalue: response.data });
                            } else {
                                this.props.onFinished();
                            }
                            //
                        }
                    })
                    .catch((e) => {
                        console.error("error =>", e);
                        this.setState({ error: true });
                        this.props.onFinished();
                    });
            } catch (err) {
                this.setState({ error: true });
            }
        } else if (e.data["stripe_checkout"]) {
            _openStripeSession(e.data["stripe_checkout"]);
        } else if (e.data.postback) {
            const client = MatrixClientPeg.get();
            client.sendMessage(this.props.mxEvent.getRoomId(), {
                body: e.data.postback,
                msgtype: "m.text",
            });
        } else if (e.data.authorize) {
            const client = MatrixClientPeg.get();
            let url = e.data.authorize.url;

            let header = { "Content-Type": "application/json" };

            client.getThreePids().then((addresses) => {
                let data = {
                    roomId: this.props.mxEvent.getRoomId(),
                    senderId: client.getUserId(),
                    baseUrl: SdkConfig.get().default_server_config[
                        "m.homeserver"
                    ].base_url,
                    ...e._processedData.authorize.data,
                };
                let emails = addresses.threepids.filter(
                    (a) => a.medium === "email"
                );

                if (emails.length > 0) {
                    data.email = emails[0].address;
                }
                axios
                    .post(url, data, header)
                    .then((response) => {
                        console.log(response);
                    })
                    .catch((error) => {
                        console.error(error);
                    });
            });
        } else if (e._processedData.hintcloud) {
            this._openCloudinaryButton(e._processedData.hintcloud);
        } else {
            this.props.onFinished();
        }
    }

    generateTrigguerId() {
        var length = 16,
            charset =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
            retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        return retVal;
    }
    navigateToCar(item) {
        let url = window.location.href + "/" + item.eventid;
        window.location.href = url;
        this.props.onFinished();
    }

    render() {
        const BaseDialog = sdk.getComponent("views.dialogs.BaseDialog");
        const Loader = sdk.getComponent("elements.Spinner");
        let spinner;
        if (this.state.loading) {
            spinner = <Loader />;
        }
        return (
            <BaseDialog
                className="mx_SetEmailDialog"
                onFinished={this.props.onFinished}
                title={this.props.title}
                contentId="mx_Dialog_content"
            >
                <div className="mx_adaptiveCard_modal">
                    {this.state.adaptivevalue && (
                        <>
                            <AdaptiveCard
                                onActionSubmit={(e) =>
                                    this.onActionSubmitAdaptive(e)
                                }
                                payload={this.state.adaptivevalue}
                                style={{ border: "none" }}
                            />
                        </>
                    )}

                    {this.state.noItems && <p>no results found</p>}
                    {this.state.error && (
                        <p style={{ textAlign: "center" }}>
                            An error has occured, check your post request
                            endpoint
                        </p>
                    )}
                    {this.state.loading && spinner}
                </div>
            </BaseDialog>
        );
    }
}
