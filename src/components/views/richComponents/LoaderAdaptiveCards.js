export default function LoaderAdaptiveCards() {
    return {
        type: "AdaptiveCard",
        body: [
            {
                type: "TextBlock",
                size: "Medium",
                weight: "Bolder",
                text: "Please wait a couple of seconds ...",
            },
            {
                type: "ImageSet",
                images: [
                    {
                        type: "Image",
                        size: "Medium",
                        url: "https://i.ibb.co/6sJG0Tq/Spinner-1s-200px-1.gif",
                        spacing: "None",
                        horizontalAlignment: "Center",
                    },
                ],
            },
        ],
        $schema: "http://adaptivecards.io/schemas/adaptive-card.json",
        version: "1.3",
    };
}
export function _openStripeSession(data) {
    const stripe = Stripe(data.key);
    stripe
        .redirectToCheckout({
            // Make the id field from the Checkout Session creation API response
            // available to this file, so you can provide it as parameter here
            // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
            sessionId: data.session_id,
        })
        .then((result) => {
            // If `redirectToCheckout` fails due to a browser or network
            // error, display the localized error message to your customer
            // using `result.error.message`.
        });
}
export function _openCloudinaryWidget(cloud) {
    let allResponse = [];
    var params = {
        cloudName: cloud.cloudname,
        uploadPreset: cloud.uploadpreset,
        sources: ["local", "camera"],
        language: cloud.language,
        text: {
            en: {
                or: "Or",
                menu: {
                    files: "My Files",
                },

                local: {
                    browse: "Browse",
                    dd_title_multi: cloud.buttontext,
                    dd_title_single: "Drag and Drop an asset here",
                },
                queue: {
                    title: "Upload Queue",
                    upload_more: "Upload more",
                    done: "Done",
                    statuses: {
                        uploaded: "Done",
                    },
                },
            },
            fr: {
                or: "Ou",
                menu: {
                    files: "Mes fichiers",
                },

                local: {
                    browse: "Parcourir",
                    dd_title_multi: cloud.buttontext,
                    dd_title_single: "Faites glisser et déposez un élément ici",
                },
                queue: {
                    title: "File d'attente de téléchargement",
                    upload_more: "Télécharger plus",
                    done: "Terminé",
                    statuses: {
                        uploaded: "Terminé",
                    },
                },
            },
        },
    };

    const myWidget = cloudinary.createUploadWidget(params, (error, result) => {
        if (!error && result && result.event === "success") {
            this._onSuccess = true;
            allResponse.push(result.info);
        }
        if (error && result.event != "success") {
        }
        if (this._onSuccess && result.event == "close") {
            var data = JSON.parse(JSON.stringify(allResponse));
            var i;
            for (i = 0; i < data.length; i++) {
                data[i]["room_id"] = this._room._roomId;
                data[i]["sender_id"] = this._ownUserId;
            }

            if (cloud.posturl) {
                var posturl = cloud.posturl;
                const header = {};
                axios
                    .post(posturl, data, header)
                    .then((res) => {})
                    .catch((error) => {
                        console.error(error);
                    });
            }
        }
    });
    myWidget.open();
}
